// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftMultihash",
    platforms: [
        .macOS(.v10_12),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SwiftMultihash",
            targets: ["SwiftMultihash"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "SwiftHex", url: "https://gitlab.com/novoco-open-libs/swift-hex.git", .branch("master")),
        .package(name: "Base58String", url: "https://github.com/novocodev/Base58String.git", .branch("master")),
        .package(name: "VarInt", url: "https://gitlab.com/novoco-open-libs/swift-varint.git", .branch("master")),
        .package(name: "CryptoSwift", url: "https://github.com/krzyzanowskim/CryptoSwift.git", .upToNextMinor(from: "1.4.0"))
        
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "SwiftMultihash",
            dependencies: ["SwiftHex", "Base58String", "VarInt", "CryptoSwift"]),
        .testTarget(
            name: "SwiftMultihashTests",
            dependencies: ["SwiftMultihash"]),
    ]
)
